import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {SignUpComponent} from './sign-up/sign-up.component';
import {SignInComponent} from './sign-in/sign-in.component';
import {UserRoutingModule} from './user-routing.module';

@NgModule({
  declarations: [SignInComponent, SignUpComponent ],
  imports: [
    CommonModule,
    FormsModule,
    UserRoutingModule, NgbModule,
    NgxDatatableModule
    //Ng4LoadingSpinnerModule.forRoot()
  ],
  exports: [
    UserRoutingModule
  ]
})
export class UserModule {
}
