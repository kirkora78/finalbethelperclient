import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../core/types/user.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  @Input() user: User;
  signupForm: FormGroup;

  constructor() {
  }

  ngOnInit() {
    this.signupForm = new FormGroup({
      'userData': new FormGroup({
        'username': new FormControl(null, [Validators.required]),
        'email': new FormControl(null, [Validators.required, Validators.email])
      }),
      // 'gender': new FormControl('male'),
      // 'hobbies': new FormArray([])
    });

  }
  onSubmit() {
    console.log(this.signupForm);
    this.signupForm.reset();
  }
}
