import {Component, OnInit} from '@angular/core';
import {User} from '../../../core/types/user.model';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  user: User;

  constructor() {
  }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form ?: NgForm) {
    form.reset();
    this.user = {
     id: 0,
  userName: '',
  password: '',
  firstName: '',
  lastName: '',
   email: ''
    };
  }

}
