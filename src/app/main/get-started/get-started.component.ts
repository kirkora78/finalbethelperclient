import { Inject, OnInit, HostListener } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-get-started',
  templateUrl: './get-started.component.html',
  styleUrls: ['./get-started.component.css']
})
export class GetStartedComponent implements OnInit {
  state: string;
  showMobile: boolean;
  showDesktop: boolean;
  navbarOpen = false;
  constructor(@Inject(DOCUMENT) document
  ) { }

  ngOnInit() {
    if (window.innerWidth < 768) {
      this.showMobile = true;
      this.showDesktop = false;
    } else {
      this.showMobile = false;
      this.showDesktop = true;
    }
  }

  @HostListener('window:resize') updateOrientatioState() {
    if (window.innerHeight > window.innerWidth) {
      this.state = 'portrait';
    } else {
      this.state = 'landscape';
    }
    console.log(this.state);
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
}
