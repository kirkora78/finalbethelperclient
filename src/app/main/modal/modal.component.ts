import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ModalService} from './modal.service';
import {DivisionsService} from '../../core/services/divisions.service';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html'
})
export class ModalComponent implements OnInit, OnDestroy {
  @Input() id: string;
  private element: any;
  countryName: string;
  allDivisions: any;
  @ViewChild('basicModal', {static: true}) basicModal: any;

  constructor(private modalService: ModalService, private el: ElementRef, private divisionsService: DivisionsService) {
    this.element = el.nativeElement;
  }

  ngOnInit(): void {
    const modal = this;

    // ensure id attribute exists
    if (!this.id) {
      console.error('modal must have an id');
      return;
    }
    // add self (this modal instance) to the modal service so it's accessible from controllers
    this.modalService.add(this);
  }

  // remove self from modal service when directive is destroyed
  ngOnDestroy(): void {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  // open modal
  open(): void {
   this.basicModal.show();
  }

  // close modal
  close(): void {
    this.basicModal.hide();
  }

  getAllDivisionsByCountry() {

    this.divisionsService.getAllDivisionsByCountry(this.countryName).subscribe({
      next: (response) => {
        // debugger;
        this.allDivisions = response;
        console.log(response);
      },
      error: (error) => {

      }
    });
  }

  divisionSelect(event) {
    this.countryName = event.target.value;
    this.getAllDivisionsByCountry();

  }
}
