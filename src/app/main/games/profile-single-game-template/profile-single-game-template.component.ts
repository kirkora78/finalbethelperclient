import { Component, Input} from '@angular/core';

import { TeamRealTimeView } from 'src/app/core/types/server/team';
import {ActivatedRoute} from '@angular/router';
import {GamesService} from '../../../core/services/games.service';
import {NewPlayerEntryPushModel, PlayerProfileModel} from '../../../core/types/server/player';
import {TeamProfileModel} from '../../../core/types/server/team';
import {RedCardEventModel} from '../../../core/types/server/redcard';

@Component({
  selector: 'app-profile-single-game-template',
  templateUrl: './profile-single-game-template.component.html',
  styleUrls: ['./profile-single-game-template.component.css']
})
export class ProfileSingleGameTemplateComponent {
  // @Input()
  inputModel: TeamRealTimeView;
  constructor(private activeRoute: ActivatedRoute,  private gamesService: GamesService) {

    
    this.inputModel = {
      name: '',
    nick: '',
    teamProfiles:  [],
    currentProfile: {
      gameProfileId: 0,
    ownGoals: '',
    otherGoals:  '',
    matchSituation: '',
    resultOutcomeLetter:  '',
    rivalName: '',
    missedPen:  0,
    savedPen:  0,
    goalLines: 0,
    isSelected: false,
    playersMatchSchema:  '',
    gameDateDay:  '',
    gameDateMonth:  '',
    newManager:  '',
    currentFormLevel:  0,
    currentRivalFormLevel:  0,
    currentFormDetails:  '',
    currentFormRivalDetails:  '',
    isMileStone: false,
    visibilityMode: false,
    woodWorks:  '',
    redCardEvents: [],
    playersIds: []
    },
    players: [],
    newPlayers: []
    };
  }
}

