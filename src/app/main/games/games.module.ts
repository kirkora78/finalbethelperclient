import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {GamesRoutingModule} from './games-routing.module';
import {GameListComponent} from './game-list/game-list.component';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { ProfileSingleGameTemplateComponent } from './profile-single-game-template/profile-single-game-template.component';
import { SingleGameComponent } from './single-game/single-game.component';
import {MDBBootstrapModule} from 'angular-bootstrap-md';

@NgModule({
  declarations: [GameListComponent, ProfileSingleGameTemplateComponent, SingleGameComponent ],
  imports: [
    CommonModule,
    FormsModule,
    GamesRoutingModule, NgbModule,
    MDBBootstrapModule.forRoot()
    //Ng4LoadingSpinnerModule.forRoot()
  ],
  exports: [
    GamesRoutingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GamesModule {
}
