import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GamesService } from 'src/app/core/services/games.service';
import { GameSingleFullView } from 'src/app/core/types/server/game';
import { TeamRealTimeView } from 'src/app/core/types/server/team';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-single-game',
  templateUrl: './single-game.component.html',
  styleUrls: ['./single-game.component.css']
})
export class SingleGameComponent implements OnInit {
  game: GameSingleFullView
  gameId: string;
  hostModel: TeamRealTimeView;
  guestModel: TeamRealTimeView;
  isLoaded: boolean;
  host:any;
  guest:any;
  dateForGame: any;
  status:any;
  constructor(private activeRoute: ActivatedRoute, private gamesService: GamesService) { this.isLoaded = false; }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => {
      this.gameId = params['gameId'];
      //.subscribe(heroes => this.heroes = heroes);
      this.gamesService.getGameById(this.gameId)
        .subscribe({
          next: (response: GameSingleFullView) => {
            console.log(response);
            this.host = response.hostModel.name;
            this.guest = response.guestModel.name;
           // this.hostModel = response.hostModel;
           // this.guestModel = response.guestModel;
            this.dateForGame = response.dateForGame;
            this.status = response.status;
            console.log(this.host)
            this.isLoaded = true;
          },
          error: (error) => {

          }
        })
    });
  }

}
