import {Routes, RouterModule} from '@angular/router';
import {GameListComponent} from './game-list/game-list.component';
import {NgModule} from '@angular/core';
import { SingleGameComponent } from './single-game/single-game.component';


const routes: Routes = [
  {
    path: '',
    component: GameListComponent
  },
  {
    path: ':gameId',
    component:  SingleGameComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GamesRoutingModule {
}
