import {ElementRef, EventEmitter, Inject, Input, NgZone, OnInit, Output} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {DivisionsService} from '../../core/services/divisions.service';
import {ModalService} from '../modal/modal.service';
interface  Country {
  name: string;
  divisions: Division2[];
  checked: boolean;

}
interface  Division2 {
  name: string;
  // countryName: string;
  checked2: boolean;

}
@Component({
  selector: 'app-leagues',
  templateUrl: './leagues.component.html',
  styleUrls: ['./leagues.component.css']
})
export class LeaguesComponent implements OnInit {

  navbarOpen = false;
  divisionsEngland: any;
  divisionsGermany: any;
  divisionsFrance: any;
  checkedOptions: any;
  checkedOptions2: any;
  @Input() options = Array<Country>();

  @Input() selectedValues: string[];
  @Input() selectedCountries: string[];
  @Input() selectedDivisions: string[];
  @Output() toggle = new EventEmitter<any[]>();
  constructor(@Inject(DOCUMENT) document, private eRef: ElementRef, private router: Router, private divisionsService: DivisionsService,
              public zone: NgZone, private modalService: ModalService) {

    // this.division = {
    //   name: '',
    //   country: ''
    // };
    // this.navbarOpen = false;
    // const element = document.getElementById("collapseOne");
    // this.isArrowKeyPressed = true;
    this.divisionsEngland = [
      {
        name: 'efl',
       // divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false
      },
      {
        name: 'Second Division',
        // divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false
      },
      {
        name: 'Third Division',
        // divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false
      },
      {
        name: 'Fourth Division',
        // divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false
      },
      {
        name: 'Fifth Division',
        // divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false
      }
      ];

    this.divisionsGermany = [
      {
        name: 'First Division',
        // divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false
      },
      {
        name: 'Second Division',
        // divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false
      },
      {
        name: 'Third Division',
        // divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false
      }
    ];

    this.divisionsFrance = [
      {
        name: 'First Division',
        // divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false
      },
      {
        name: 'Second Division',
        // divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false
      },
      {
        name: 'Third Division',
        // divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false
      }
    ];
    this.options = [
      {
        name: 'England',
        divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false

      },
      {
        name: 'Germany',
        divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],

        checked: false


      },
      {
        name: 'France',
        divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false

      },
      {
        name: 'Italy',
        divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],

        checked: false


      },
      {
        name: 'Spain',
        divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],

        checked: false


      }
    ];
    //  this.options.name = ['England', 'England', 'England', 'England', 'England'];
  }

  ngOnInit() {

  }

  onToggle() {
    this.checkedOptions = this.divisionsEngland.filter(x => x.checked);
    //this.checkedOptions2 = this.options.filter(x => x.checked).map(x => x.divisions);
    this.selectedValues = this.checkedOptions.map(x => x.name);
    this.selectedDivisions = this.checkedOptions.map(x => x.divisions);

    this.toggle.emit(this.checkedOptions.map(x => x.divisions));
    console.log(this.checkedOptions);
    console.log(this.checkedOptions2);
    console.log(this.selectedValues);
    console.log(this.selectedDivisions);
    // console.log(checkedOptions.find(x => x.divisions))
  }

  onToggle2() {
    this.checkedOptions2 =  this.options[0].divisions.filter(x => x.checked2);
    this.selectedCountries = this.checkedOptions2.map(x => x.name);
    // this.selectedDivisions = checkedDivisions.map(x => x.countryName);
    this.toggle.emit(this.checkedOptions2.map(x => x.name));
    console.log(this.checkedOptions2);
    console.log(this.selectedCountries);
    console.log(this.selectedDivisions);
    // console.log(checkedOptions.find(x => x.divisions))
  }
  saveChanges() {
    console.log('save')
  }
  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
}
