import {MainRoutingModule} from './main-routing.module';
import {CommonModule} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {TeamsService} from '../core/services/teams.services';
import {HomeService} from '../core/services/home.service';
import {GamesService} from '../core/services/games.service';
import {MainComponent} from './main/main.component';
import {SubscriptionsComponent} from './subscriptions/subscriptions.component';
import {HomeComponent} from './home/home.component';
import {StrategiesComponent} from './strategies/strategies.component';
import {AdminFormsService} from '../core/services/admin-forms/admin-forms.service';
import {FormsModule} from '@angular/forms';
import {MDBBootstrapModule, ModalModule} from 'angular-bootstrap-md';
import {BrowserModule} from '@angular/platform-browser';
import {DivisionsService} from '../core/services/divisions.service';
import {MenuComponent} from './menu/menu.component';
import {ModalService} from './modal/modal.service';
import {ModalComponent} from './modal/modal.component';
import {LeaguesComponent} from './leagues/leagues.component';
import {GetStartedComponent} from './get-started/get-started.component';
//import {MatButtonModule, MatFormFieldModule, MatInputModule, MatRippleModule} from '@angular/material';
// MDB Angular Free
// import { ModalModule, TooltipModule, PopoverModule, ButtonsModule } from 'angular-bootstrap-md';
// const modules = [
//   MatButtonModule,
//   MatFormFieldModule,
//   MatInputModule,
//   MatRippleModule
// ];
@NgModule({
  declarations: [MainComponent, SubscriptionsComponent, HomeComponent, StrategiesComponent, MenuComponent, ModalComponent, LeaguesComponent, GetStartedComponent],
  imports: [
    CommonModule,
    FormsModule,
    // RouterModule
    BrowserModule,
    MainRoutingModule,
    NgxDatatableModule,
    // NgDatepickerModule,
    NgbModule,
   // ...modules
    MDBBootstrapModule.forRoot(),
    ModalModule.forRoot(),
  ],
  exports: [MainRoutingModule],
  providers: [
    TeamsService,
    GamesService,
    HomeService,
    AdminFormsService,
    DivisionsService,
    ModalService
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass:,
    //   multi: true
    // }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MainModule {}

