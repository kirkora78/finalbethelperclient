import {Component, Inject, OnInit} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {HomeService} from '../../core/services/home.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

 // navbarOpen = false;
  countries: any[];
  constructor(private router: Router, private homeService: HomeService, @Inject(DOCUMENT) document
  ) {
  }

  ngOnInit() {
    this.homeService.getAllCountries().subscribe(
          (response: any[]) => {
            this.countries = response;
            console.log(response);

          },
          (error) => {

          }
        );


  }
}
