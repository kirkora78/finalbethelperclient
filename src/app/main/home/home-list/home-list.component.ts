// import {ActivatedRoute, Router} from '@angular/router';
// import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
// import {Component, OnInit} from '@angular/core';
// import {GamesService} from '../../../core/services/games.service';
// import {HomeService} from '../../../core/services/home.service';
//
//
// @Component({
//   selector: 'app-game-list',
//   templateUrl: './home-list.component.html',
//   styleUrls: ['./home-list.component.css']
// })
// export class HomeListComponent implements OnInit {
//   countries: any[];
//   country: any;
//   games: any[];
//   datasets: any[];
//   gamesList = ['Windstorm', 'Bombasto', 'Magneta', 'Tornado'];
//   predictionsList = ['Windstorm', 'Bombasto', 'Magneta', 'Tornado'];
//   gamesListLiveScore = ['Windstorm', 'Bombasto', 'Magneta'];
//   constructor(private activeRoute: ActivatedRoute, private router: Router, private homeService: HomeService,
//               private modalService: NgbModal) {
//   }
//
//   ngOnInit() {
//
//       this.activeRoute.queryParams.subscribe(params => {
//
//         // this.homeService.getAllCountries().subscribe(
//         //   (response: any[]) => {
//         //     this.countries = response;
//         //     console.log(response);
//         //
//         //   },
//         //   (error) => {
//         //
//         //   }
//         // );
//
//       });
//
//   }
//
//   // public doughnutChartLabels:string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
//   // public doughnutChartData:number[] = [350, 450, 100];
//   // public doughnutChartType:string = 'doughnut';
//   //
//   // // events
//   // public chartClicked(e:any):void {
//   //   console.log(e);
//   // }
//   //
//   // public chartHovered(e:any):void {
//   //   console.log(e);
//   // }
//   // public barChartLabels:string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
//   // public barChartType:string = 'bar';
//   // public barChartLegend:boolean = true;
//   //
//   // // public barChartData:any[] = [
//   // //   {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
//   // //   {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
//   // // ];
//   //
//   // lineChart
//   public lineChartData:Array<any> = [
//     {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
//     {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
//     {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'},
//     // {data: [65, 59, 80, 81, 56, 55, 40], },
//     // {data: [28, 48, 40, 19, 86, 27, 90], },
//     // {data: [18, 48, 77, 9, 100, 27, 40], }
//   ];
//   public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June'];
//   public lineChartOptions:any = {
//     scaleShowVerticalLines: false,
//     responsive: true
//   };
//   public lineChartColors:Array<any> = [
//     { // grey
//       backgroundColor: '#c5a9ff',
//       borderColor: 'rgba(148,159,177,1)',
//       pointBackgroundColor: 'rgba(148,159,177,1)',
//       pointBorderColor: '#262626',
//       pointHoverBackgroundColor: '#7188ff',
//       pointHoverBorderColor: 'rgba(148,159,177,0.8)'
//     },
//     { // dark grey
//       backgroundColor: '#7188ff',
//       borderColor: 'rgba(77,83,96,1)',
//       pointBackgroundColor: 'rgba(77,83,96,1)',
//       pointBorderColor: '#262626',
//       pointHoverBackgroundColor: '#acffe5',
//       pointHoverBorderColor: 'rgba(77,83,96,1)'
//     },
//     { // grey
//       backgroundColor: '#ffc09a',
//       borderColor: 'rgba(148,159,177,1)',
//       pointBackgroundColor: 'rgba(148,159,177,1)',
//       pointBorderColor: '#353535',
//       pointHoverBackgroundColor: '#7188ff',
//       pointHoverBorderColor: 'rgba(148,159,177,0.8)'
//     }
//   ];
//   public lineChartLegend:boolean = true;
//   public lineChartType:string = 'horizontalBar';
//   // public lineChartType:string = 'bar';
//   // public randomize():void {
//   //   let _lineChartData:Array<any> = new Array(this.lineChartData.length);
//   //   for (let i = 0; i < this.lineChartData.length; i++) {
//   //     _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
//   //     for (let j = 0; j < this.lineChartData[i].data.length; j++) {
//   //       _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
//   //     }
//   //   }
//   //   this.lineChartData = _lineChartData;
//   // }
//
//   // // events
//   // public chartClicked(e:any):void {
//   //   console.log(e);
//   // }
//   //
//   // public chartHovered(e:any):void {
//   //   console.log(e);
//   // }
//
// }
