import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { TeamsService } from '../../../core/services/teams.services';
import { ActivatedRoute } from '@angular/router';
import { SingleTeamModel } from 'src/app/core/types/server/team';
import { ActivitiesService } from 'src/app/core/services/activities.service';
import { DataTableColumnHeaderDirective } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-single-team',
  templateUrl: './single-team.component.html',
  styleUrls: ['./single-team.component.css']
})
export class SingleTeamComponent implements OnInit {
  nick: string;
  teamModel: any = {};
  initialtakeValue: number = 3;
  loadMoreSkipValue: number = 0;
  playerIds = [];
  showAccordion = false;
  showTable = true;
  showMobile: boolean;
  showDesktop: boolean;
  addControlsToCarousel: boolean;
  slides: any = [[]];
  gameProfiles = [];
  constructor(private teamsService: TeamsService, private activityService: ActivitiesService, private activeRoute: ActivatedRoute) {

  }
  // @ViewChild('carousel', { static: false }) public el: any;

  // @HostListener('swipeleft', ['$event']) public swipePrev(event: any) {
  //   this.el.previousSlide();
  // }

  // @HostListener('swiperight', ['$event']) public swipeNext(event: any) {
  //   this.el.nextSlide();
  // }

  ngOnInit() {
    if (window.innerWidth < 768) {
      this.showMobile = true;
      this.showDesktop = false;
      this.addControlsToCarousel = false;
    } else {
      this.showMobile = false;
      this.showDesktop = true;
      this.addControlsToCarousel = true;
    }
    this.activeRoute.params.subscribe(params => {
      this.nick = params['nick'];
      this.teamsService.getTeamById(this.nick, this.loadMoreSkipValue, this.initialtakeValue).subscribe({
        next: (response) => {
          debugger;
          this.teamModel = response;
        },
        error: (error) => {

        }
      });
    });
  }

  loadMore() {
    debugger;
    this.loadMoreSkipValue = this.loadMoreSkipValue + this.initialtakeValue;
    this.teamsService.getTeamById(this.nick, this.loadMoreSkipValue, this.initialtakeValue).subscribe({
      next: (response) => {
        debugger;
        this.teamModel = this.mergeModels(this.teamModel, response);
      },
      error: (error) => {

      }
    });
  }

  mergeModels(existingModel, newModel) {
    debugger;
    existingModel.teamGameProfiles = newModel.teamGameProfiles.concat(existingModel.teamGameProfiles);
    existingModel.players = this.mergePlayers(existingModel.players, newModel.players,newModel.teamGameProfiles,existingModel.teamGameProfiles).sort((a, b) => a.sortIndex - b.sortIndex);
    return existingModel;
  }

  mergePlayers(oldPlayers, newPlayers,newProfiles,oldProfiles) {
    for (var player of oldPlayers) {
      var newPlayer = newPlayers.find(x => x.playerId == player.playerId);
      if (newPlayer != null) {
        player.activities = newPlayer.activities.concat(player.activities);
      }
      else {
        debugger;
        for (let i =newProfiles.length-1; i >=0; i--) {
          player.activities.unshift({profileId:newProfiles[i].gameProfileId,minutesPerGame:'N'});
        }

      }
    }
    for (var player of newPlayers) {
      var newPlayer = oldPlayers.find(x => x.playerId == player.playerId);
      debugger;
      if (newPlayer === null || !newPlayer) {
        debugger;
        oldPlayers.push(player);
        for (let i = 0; i < this.loadMoreSkipValue; i++) {
          player.activities.push({});
        }
      }
    }

    return oldPlayers;
  }
  toggleColor(activity) {
    activity.isSelected = !activity.isSelected;
  }
  switchView() {
    this.showAccordion = !this.showAccordion;
    this.showTable = !this.showTable;
  }

  getActivityColor(activity){
    if(activity.isMilestone)
    {
      return 'green';
    }
   else if(activity.isSelected){
     return 'yellow';
   }
   else{
     return 'white'
   }
  }

  setMilestone(profile) {
    profile.isMilestone=true;
    debugger;
    let regExp = new RegExp('^[t0-9][0-9]?$');
    let players = this.teamModel.players;
    for (let i = 0; i < players.length; i++) {
      let activities = players[i].activities;
      let currentActivity = activities.find(x => x.profileId ==profile.gameProfileId);
      if(currentActivity)currentActivity.isMilestone=true;
      for (let k = 0; k < activities.length; k++) {
        let activity = activities[k];
        debugger;
        if (Number(activity.profileId) > Number(profile.gameProfileId)&&currentActivity&&regExp.test(currentActivity.minutesPerGame)) {
          if (activity === {} || !regExp.test(activity.minutesPerGame)) {
            activity.isSelected = true;
          }
        }
      }
    }
  }

  unsetMilestone(profile) {
    profile.isMilestone=false;
    let players = this.teamModel.players;
    for (let i = 0; i < players.length; i++) {
      let activities = players[i].activities;
      let currentActivity = activities.find(x => x.profileId ==profile.gameProfileId);
      if(currentActivity)currentActivity.isMilestone=false;
      for (let k = 0; k < activities.length; k++) {
        let activity = activities[k];
        debugger;
        if (Number(activity.profileId) > Number(profile.gameProfileId)&&activity.isSelected===true) {
            activity.isSelected = false;
        }
      }
    }
  }
  chunk(arr, chunkSize) {
    const R = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    return R;
  }
}
