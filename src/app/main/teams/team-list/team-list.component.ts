import { TeamsService } from '../../../core/services/teams.services';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {
  teams: any[];
  constructor(private activeRoute: ActivatedRoute, private router: Router, private teamsService: TeamsService,
    private modalService: NgbModal) { }
  //divisionName="championship";
  ngOnInit() {
    //this.getAllTeams();
    //  this.getTeamDivision();
    this.activeRoute.queryParams.subscribe(params => {
      this.teamsService.getAllTeams().subscribe(
        (response: any[]) => {
          this.teams = response;
          console.log(response);
        },
        (error) => {

        }
      );

    });
  }

  getTeamDivision() {
    // this.teamsService.getAllTeams().subscribe(
    //   (response: any[]) => {
    //     this.teams = response;
    //     console.log(response);
    //   },
    //   (error) => {
    //
    //   }
    // );

    // this.activeRoute.params.subscribe(params => {
    //  // this.divisionName = params['name'];
    //   this.teamsService.getTeamDivision(this.divisionName).subscribe({
    //     next: (response) => {
    //       debugger;
    //     },
    //     error: (error) => {
    //
    //     }
    //   });
    // });
  }

}
