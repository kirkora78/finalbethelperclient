import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TeamListComponent} from './team-list/team-list.component';
import {SingleTeamComponent} from './single-team/single-team.component';

const routes: Routes = [
  {
    path: '',
    component: TeamListComponent
  },
  {
    path: ':nick',
    component: SingleTeamComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TeamsRoutingModule {
}
