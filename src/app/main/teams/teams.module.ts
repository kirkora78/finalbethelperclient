import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TeamListComponent} from './team-list/team-list.component';
import {TeamsRoutingModule} from './teams-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SingleTeamComponent } from './single-team/single-team.component';
import {MDBBootstrapModule} from 'angular-bootstrap-md';

@NgModule({
  declarations: [TeamListComponent, SingleTeamComponent ],
  imports: [
    CommonModule,
    FormsModule,
    TeamsRoutingModule, NgbModule,
    NgxDatatableModule,
    MDBBootstrapModule.forRoot()
    //Ng4LoadingSpinnerModule.forRoot()
  ],
  exports: [
    TeamsRoutingModule
  ]
})
export class TeamsModule {
}
