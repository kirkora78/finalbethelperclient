import {ElementRef, HostListener, NgZone, OnInit, ViewChild} from '@angular/core';
import {Component} from '@angular/core';
import {ModalService} from '../modal/modal.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  menuState: string = 'out';
  basicModal: any;

  constructor(public zone: NgZone, private modalService: ModalService) { }

  ngOnInit() {
  }

  openModal(id: string) {
    this.modalService.open(id);
  }
  closeModal(id: string) {
    this.modalService.close(id);
  }

  clickedOutside($event: Event) {
    $event.preventDefault();
    $event.stopPropagation();
    if (this.menuState === 'in') {
      this.toggleMenu();
    }
   console.log("CLICKED INSIDE, MENU WON'T HIDE");
  }

  toggleMenu() {
    this.zone.run(() => {
      this.menuState = this.menuState === 'out' ? 'in' : 'out';
    });
  }
}
