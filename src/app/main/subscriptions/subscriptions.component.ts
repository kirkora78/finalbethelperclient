import {Inject, OnInit} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {Component} from '@angular/core';

@Component({
  selector: "app-subscriptions",
  templateUrl: "./subscriptions.component.html",
  styleUrls: ["./subscriptions.component.css"]
})
export class SubscriptionsComponent implements OnInit {

  navbarOpen = false;

  constructor(@Inject(DOCUMENT) document
  ) {
  }

  ngOnInit() {

  }
}
