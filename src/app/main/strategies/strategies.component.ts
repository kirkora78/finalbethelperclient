import {Inject, OnInit} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {Component} from '@angular/core';

@Component({
  selector: 'app-strategies',
  templateUrl: './strategies.component.html',
  styleUrls: ['./strategies.component.css']
})
export class StrategiesComponent implements OnInit {

  navbarOpen = false;

  constructor(@Inject(DOCUMENT) document
  ) {
  }

  ngOnInit() {

  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
}
