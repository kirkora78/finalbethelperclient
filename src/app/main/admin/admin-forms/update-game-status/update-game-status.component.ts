import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AdminFormsService } from 'src/app/core/services/admin-forms/admin-forms.service';

@Component({
  selector: 'app-update-game-status',
  templateUrl: './update-game-status.component.html',
  styleUrls: ['./update-game-status.component.css']
})
export class UpdateGameStatusComponent implements OnInit {
  formData: FormGroup;
  GameId: FormControl;
  Year: FormControl;
  Month: FormControl;
  Day: FormControl;
  Hour: FormControl;
  Min: FormControl;

  constructor(private adminFormsService: AdminFormsService) { }

  ngOnInit() {
    this.formData = new FormGroup({
      GameId: new FormControl(''),
      Year: new FormControl(''),
      Month: new FormControl(''),
      Day: new FormControl(''),
      Hour: new FormControl(''),
      Min: new FormControl('')
    });
  }

  submitForm(data) {
    this.adminFormsService.updateGameStatus(data).subscribe();
  }
}
