import {FormControl, FormGroup} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AdminFormsService } from 'src/app/core/services/admin-forms/admin-forms.service';

@Component({
  selector: 'app-update-undefined-profile',
  templateUrl: './update-undefined-profile.component.html',
  styleUrls: ['./update-undefined-profile.component.css']
})
export class UpdateUndefinedProfileComponent implements OnInit {
  formData : FormGroup;
  ProfileId: FormControl;
  UndefinedId : FormControl;
  PlayerId : FormControl;
  PlayerNewName : FormControl;

  constructor(private adminFormsService: AdminFormsService) { }

  ngOnInit() {
    this.formData = new FormGroup({
      ProfileId: new FormControl(''),
      UndefinedId: new FormControl(''),
      PlayerId: new FormControl(''),
      PlayerNewName: new FormControl('')
    });
  }

  submitForm(data) {
    this.adminFormsService.updateUndefinedProfile(data).subscribe();
    this.formData.reset();
  }

}
