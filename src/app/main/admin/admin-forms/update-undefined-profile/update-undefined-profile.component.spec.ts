import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUndefinedProfileComponent } from './update-undefined-profile.component';

describe('UpdateUndefinedProfileComponent', () => {
  let component: UpdateUndefinedProfileComponent;
  let fixture: ComponentFixture<UpdateUndefinedProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateUndefinedProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUndefinedProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
