import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDuplicateProfileComponent } from './update-duplicate-profile.component';

describe('UpdateDuplicateProfileComponent', () => {
  let component: UpdateDuplicateProfileComponent;
  let fixture: ComponentFixture<UpdateDuplicateProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDuplicateProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDuplicateProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
