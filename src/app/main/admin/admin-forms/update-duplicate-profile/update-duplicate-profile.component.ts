import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import { AdminFormsService } from 'src/app/core/services/admin-forms/admin-forms.service';

@Component({
  selector: 'app-update-duplicate-profile',
  templateUrl: './update-duplicate-profile.component.html',
  styleUrls: ['./update-duplicate-profile.component.css']
})
export class UpdateDuplicateProfileComponent implements OnInit {
  formData : FormGroup;
  FirstPlayerId: FormControl;
  FirstPlayerName : FormControl;
  SecondPlayerId : FormControl;
  SecondPlayerName : FormControl;

  constructor(private adminFormsService: AdminFormsService) { }

  ngOnInit() {
    this.formData = new FormGroup({
      FirstPlayerId : new FormControl(''),
      FirstPlayerName: new FormControl(''),
      SecondPlayerId: new FormControl(''),
      SecondPlayerName: new FormControl('')
    });
  }

  submitForm(data) {
    this.adminFormsService.updateDuplicateProfile(data).subscribe();
    this.formData.reset();
  }

}
