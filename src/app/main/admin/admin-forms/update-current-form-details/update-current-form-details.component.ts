import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AdminFormsService } from 'src/app/core/services/admin-forms/admin-forms.service';

@Component({
  selector: 'app-update-current-form-details',
  templateUrl: './update-current-form-details.component.html',
  styleUrls: ['./update-current-form-details.component.css']
})
export class UpdateCurrentFormDetailsComponent implements OnInit {
  formData: FormGroup;
  id: FormControl;
  newContent: FormControl;
  constructor(private adminFormsService: AdminFormsService) { }

  ngOnInit() {
    this.formData = new FormGroup({
      id: new FormControl(''),
      newContent: new FormControl('')
    });
  }

  submitForm(data) {
    this.adminFormsService.updateCurrentFormDetails(data).subscribe();
    this.formData.reset();
  }

}
