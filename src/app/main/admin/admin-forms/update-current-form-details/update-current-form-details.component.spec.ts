import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCurrentFormDetailsComponent } from './update-current-form-details.component';

describe('UpdateCurrentFormDetailsComponent', () => {
  let component: UpdateCurrentFormDetailsComponent;
  let fixture: ComponentFixture<UpdateCurrentFormDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCurrentFormDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCurrentFormDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
