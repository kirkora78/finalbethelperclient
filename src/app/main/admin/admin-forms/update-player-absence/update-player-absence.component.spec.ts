import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatePlayerAbsenceComponent } from './update-player-absence.component';

describe('UpdatePlayerAbsenceComponent', () => {
  let component: UpdatePlayerAbsenceComponent;
  let fixture: ComponentFixture<UpdatePlayerAbsenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatePlayerAbsenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatePlayerAbsenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
