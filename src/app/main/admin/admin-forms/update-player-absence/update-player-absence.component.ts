import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AdminFormsService } from 'src/app/core/services/admin-forms/admin-forms.service';

@Component({
  selector: 'app-update-player-absence',
  templateUrl: './update-player-absence.component.html',
  styleUrls: ['./update-player-absence.component.css']
})
export class UpdatePlayerAbsenceComponent implements OnInit {
  formData: FormGroup;
  ProfileId: FormControl;
  PlayerId: FormControl;
  AbsenceReason: FormControl;

  constructor(private adminFormsService: AdminFormsService) { }

  ngOnInit() {
    this.formData = new FormGroup({
      ProfileId: new FormControl(''),
      PlayerId: new FormControl(''),
      AbsenceReason: new FormControl('')
    });
  }

  submitForm(data) {
    this.adminFormsService.updatePlayerAbsence(data).subscribe();
    this.formData.reset();
  }

}
