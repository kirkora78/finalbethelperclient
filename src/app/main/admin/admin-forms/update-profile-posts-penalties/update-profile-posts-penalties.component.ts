import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AdminFormsService } from 'src/app/core/services/admin-forms/admin-forms.service';

@Component({
  selector: 'app-update-profile-posts-penalties',
  templateUrl: './update-profile-posts-penalties.component.html',
  styleUrls: ['./update-profile-posts-penalties.component.css']
})
export class UpdateProfilePostsPenaltiesComponent implements OnInit {
  formData: FormGroup;
  ProfileId: FormControl;
  Posts: FormControl;
  ReceivedPosts: FormControl;
  MissedPenalties: FormControl;
  SavedPenalties: FormControl;

  constructor(private adminFormsService: AdminFormsService) { }

  ngOnInit() {
    this.formData = new FormGroup({
      ProfileId: new FormControl(''),
      Posts: new FormControl(''),
      ReceivedPosts: new FormControl(''),
      MissedPenalties: new FormControl(''),
      SavedPenalties: new FormControl('')
    });
  }

  submitForm(data) {
    this.adminFormsService.updateProfilePostsandPenalties(data).subscribe();
    this.formData.reset();
  }

}
