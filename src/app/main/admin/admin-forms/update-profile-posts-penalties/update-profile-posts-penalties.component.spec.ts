import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateProfilePostsPenaltiesComponent } from './update-profile-posts-penalties.component';

describe('UpdateProfilePostsPenaltiesComponent', () => {
  let component: UpdateProfilePostsPenaltiesComponent;
  let fixture: ComponentFixture<UpdateProfilePostsPenaltiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateProfilePostsPenaltiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateProfilePostsPenaltiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
