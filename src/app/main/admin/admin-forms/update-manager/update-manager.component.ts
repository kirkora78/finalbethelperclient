import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AdminFormsService } from 'src/app/core/services/admin-forms/admin-forms.service';

@Component({
  selector: 'app-update-manager',
  templateUrl: './update-manager.component.html',
  styleUrls: ['./update-manager.component.css']
})
export class UpdateManagerComponent implements OnInit {
  formData: FormGroup;
  ProfileId: FormControl;
  NewManager: FormControl;
  constructor(private adminFormsService: AdminFormsService) { }

  ngOnInit() {
    this.formData = new FormGroup({
      ProfileId: new FormControl(''),
      NewManager: new FormControl('')
    });
  }

  submitForm(data){
    this.adminFormsService.updateManager(data).subscribe();
    this.formData.reset();
  }

}
