import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AdminFormsService } from 'src/app/core/services/admin-forms/admin-forms.service';

@Component({
  selector: 'app-update-current-form-level',
  templateUrl: './update-current-form-level.component.html',
  styleUrls: ['./update-current-form-level.component.css']
})
export class UpdateCurrentFormLevelComponent implements OnInit {
  formData: FormGroup;
  id: FormControl;
  newContent: FormControl;
  constructor(private adminFormsService: AdminFormsService) { }

  ngOnInit() {
    this.formData = new FormGroup({
      id: new FormControl(''),
      newContent: new FormControl('')
    });
  }

  submitForm(data){
    this.adminFormsService.updateCurrentFormLevel(data).subscribe();
    this.formData.reset();
  }

}
