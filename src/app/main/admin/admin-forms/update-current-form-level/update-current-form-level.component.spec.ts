import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCurrentFormLevelComponent } from './update-current-form-level.component';

describe('UpdateCurrentFormLevelComponent', () => {
  let component: UpdateCurrentFormLevelComponent;
  let fixture: ComponentFixture<UpdateCurrentFormLevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCurrentFormLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCurrentFormLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
