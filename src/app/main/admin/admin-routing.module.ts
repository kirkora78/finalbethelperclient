import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminMainComponent } from './admin-main/admin-main.component';
import { UpdateDuplicateProfileComponent } from './admin-forms/update-duplicate-profile/update-duplicate-profile.component';
import { UpdateUndefinedProfileComponent } from './admin-forms/update-undefined-profile/update-undefined-profile.component';
import { UpdateCurrentFormLevelComponent } from './admin-forms/update-current-form-level/update-current-form-level.component';
import { UpdateCurrentFormDetailsComponent } from './admin-forms/update-current-form-details/update-current-form-details.component';
import { UpdateManagerComponent } from './admin-forms/update-manager/update-manager.component';
import { UpdateProfilePostsPenaltiesComponent } from './admin-forms/update-profile-posts-penalties/update-profile-posts-penalties.component';
import { UpdateGameStatusComponent } from './admin-forms/update-game-status/update-game-status.component';
import { UpdatePlayerAbsenceComponent } from './admin-forms/update-player-absence/update-player-absence.component';
import { UpdateMilestoneComponent } from './admin-forms/update-milestone/update-milestone.component';

const routes: Routes = [
  {
    path: '',
    component: AdminMainComponent
  },
  {
    path: 'UpdateDuplicateProfile',
    component: UpdateDuplicateProfileComponent
  },
  {
    path: 'UpdateUndefinedProfile',
    component: UpdateUndefinedProfileComponent
  },
  {
    path: 'UpdateCurrentFormLevel',
    component: UpdateCurrentFormLevelComponent
  },
  {
    path: 'UpdateCurrentFormDetails',
    component: UpdateCurrentFormDetailsComponent
  },
  {
    path: 'UpdateManager',
    component: UpdateManagerComponent
  },
  {
    path: 'UpdateProfilePostsandPenalties',
    component: UpdateProfilePostsPenaltiesComponent
  },
  {
    path: 'UpdateGameStatus',
    component: UpdateGameStatusComponent
  },
  {
    path: 'UpdatePlayerAbsence',
    component: UpdatePlayerAbsenceComponent
  },
  {
    path: 'UpdateMilestone',
    component: UpdateMilestoneComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
