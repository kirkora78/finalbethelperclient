import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminMainComponent } from './admin-main/admin-main.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UpdateDuplicateProfileComponent } from './admin-forms/update-duplicate-profile/update-duplicate-profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateUndefinedProfileComponent } from './admin-forms/update-undefined-profile/update-undefined-profile.component';
import { UpdateCurrentFormLevelComponent } from './admin-forms/update-current-form-level/update-current-form-level.component';
import { UpdateCurrentFormDetailsComponent } from './admin-forms/update-current-form-details/update-current-form-details.component';
import { UpdateManagerComponent } from './admin-forms/update-manager/update-manager.component';
import { UpdateProfilePostsPenaltiesComponent } from './admin-forms/update-profile-posts-penalties/update-profile-posts-penalties.component';
import { UpdateGameStatusComponent } from './admin-forms/update-game-status/update-game-status.component';
import { UpdatePlayerAbsenceComponent } from './admin-forms/update-player-absence/update-player-absence.component';
import { UpdateMilestoneComponent } from './admin-forms/update-milestone/update-milestone.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    NgbModule
  ],
  declarations: [AdminMainComponent, UpdateDuplicateProfileComponent, UpdateUndefinedProfileComponent, UpdateCurrentFormLevelComponent, UpdateCurrentFormDetailsComponent, UpdateManagerComponent, UpdateProfilePostsPenaltiesComponent, UpdateGameStatusComponent, UpdatePlayerAbsenceComponent, UpdateMilestoneComponent]
})
export class AdminModule { }
