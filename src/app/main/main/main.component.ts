import {Component, ElementRef, EventEmitter, HostListener, Inject, Input, NgZone, OnInit, Output, ViewChild} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { NgbDropdown, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TeamProfileModel } from '../../core/types/server/team';
import { Team } from '../../core/types/server/team/team';
import { TeamsService } from '../../core/services/teams.services';
import { Division } from '../../core/types/server/division';
import { DivisionsService } from '../../core/services/divisions.service';
import { ModalDirective } from 'angular-bootstrap-md';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { ModalService } from '../modal/modal.service';

interface  Country {
  name: string;
  divisions: Division2[];
  checked: boolean;

}
interface  Division2 {
  name: string;
  // countryName: string;
  checked2: boolean;

}
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  animations: [
    trigger('slideInOut', [
      state('in', style({
        // opacity: '1',
        // display: 'block',
        transform: 'translate3d(-100%, 0, 0)'
      })),
      state('out', style({
        // opacity: '0',
        // display: 'none',
        transform: 'translate3d(0, 0,0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
  ]
})


export class MainComponent implements OnInit {
  navbarOpen: boolean;
  value: string;
  countries = ['England', 'Germany', 'Spain', 'France', 'Italy'];
  divisions = ['First Division', 'Second Division', 'Third Division', 'Fourth Division', 'Fifth Division'];
  country: any;
  division: Division;
  countryName: string;
  allDivisions: any;
  menuState = 'out';
  isArrowKeyPressed: boolean;
  marked = false;
  theCheckbox = false;
  GermanyCheckbox = false;
  checkedOptions: any;
  checkedOptions2: any;
  @Input() options = Array<Country>();

  @Input() selectedValues: string[];
  @Input() selectedCountries: string[];
  @Input() selectedDivisions: string[];
  @Output() toggle = new EventEmitter<any[]>();
  constructor(@Inject(DOCUMENT) document, private eRef: ElementRef, private router: Router, private divisionsService: DivisionsService,
               public zone: NgZone,private modalService: ModalService) {

    this.division = {
      name: '',
      country: ''
    };
    this.navbarOpen = false;
   // const element = document.getElementById("collapseOne");
   // this.isArrowKeyPressed = true;


    this.options = [
      {
      name: 'England',
      divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false

    },
      {
        name: 'Germany',
        divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],

        checked: false


      },
      {
        name: 'France',
        divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],
        checked: false

      },
      {
        name: 'Italy',
        divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],

        checked: false


      },
      {
        name: 'Spain',
        divisions: [{name: 'First Division', checked2:  true}, {name: 'Second Division', checked2: true}, { name: 'Third Division', checked2: true},{name: 'Fourth Division', checked2: true}, { name: 'Fifth Division', checked2: true}],

        checked: false


      }
    ];
  //  this.options.name = ['England', 'England', 'England', 'England', 'England'];
  }

  ngOnInit() {

  }

  onToggle() {
    this.checkedOptions = this.options.filter(x => x.checked);
    //this.checkedOptions2 = this.options.filter(x => x.checked).map(x => x.divisions);
    this.selectedValues = this.checkedOptions.map(x => x.name);
    this.selectedDivisions = this.checkedOptions.map(x => x.divisions);

    this.toggle.emit(this.checkedOptions.map(x => x.divisions));
    console.log(this.checkedOptions);
    console.log(this.checkedOptions2);
    console.log(this.selectedValues);
    console.log(this.selectedDivisions);
   // console.log(checkedOptions.find(x => x.divisions))
  }

  onToggle2() {
    this.checkedOptions2 =  this.options[0].divisions.filter(x => x.checked2);
    this.selectedCountries = this.checkedOptions2.map(x => x.name);
    // this.selectedDivisions = checkedDivisions.map(x => x.countryName);
    this.toggle.emit(this.checkedOptions2.map(x => x.name));
    console.log(this.checkedOptions2);
    console.log(this.selectedCountries);
    console.log(this.selectedDivisions);
    // console.log(checkedOptions.find(x => x.divisions))
  }
  toggleVisibility(e) {
    this.marked = e.target.checked;
    console.log(e.target.checked);
    console.log(e.target.value);
    console.log(e);
  }

  // openModal(id: string) {
  //   this.modalService.open(id);
  // }
  //
  // closeModal(id: string) {
  //   this.modalService.close(id);
  // }

  clickedInside($event: Event) {
  //  $event.preventDefault();
  //  $event.stopPropagation();

    // <- that will stop propagation on lower layers

    if (this.menuState === 'in') {
      this.toggleMenu();
    }
console.log("CLICKED OUTSIDE, MENU SHOULD HIDE");
  }
  clickIn($event: Event) {
    $event.preventDefault();
    $event.stopPropagation();

    // <- that will stop propagation on lower layers

    this.menuState = this.menuState === 'out' ? 'in' : 'out';

    console.log('CLICKED ON AN ITEM, MENU SHOULD HIDE');
  }

  toggleMenu() {

    this.zone.run(() => {
      this.menuState = this.menuState === 'out' ? 'in' : 'out';
    });
  }


  getAllDivisionsByCountry() {

    this.divisionsService.getAllDivisionsByCountry(this.countryName).subscribe({
      next: (response) => {
        // debugger;
        this.allDivisions = response;
        console.log(response);
      },
      error: (error) => {

      }
    });
  }


  divisionSelect(event) {
    this.countryName = event.target.value;
    this.getAllDivisionsByCountry();
console.log(this.countryName);
  }

  saveChanges() {
    console.log('save')
  }
// test() {
//   this.element = document.getElementById("collapseOne");
//   console.log(this.element.className)
//   console.log(this.element.classList.value)
//   if (this.element.classList.value === 'collapse') {
//     this.isArrowKeyPressed = true;
//     // const arrowOne = document.getElementById("arrowOne");
//     //  arrowOne.classList.add("test")
//     //   console.log(this.isArrowKeyPressed)
//   } else if (this.element.classList.value === 'collapse show') {
//     this.isArrowKeyPressed = false;
//     //  const arrowOne = document.getElementById("arrowOne");
//     //  arrowOne.classList.remove("test")
//   }
// }

  // @HostListener('window:scroll', ['$event'])
  // onWindowScroll(e) {
  //   if (window.pageYOffset > 160) {
  //     const element = document.getElementById('navbar');
  //     element.classList.add('sticky');
  //   } else {
  //     const element = document.getElementById('navbar');
  //     element.classList.remove('sticky');
  //   }
  // }
}
