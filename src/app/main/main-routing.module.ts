import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MainComponent} from './main/main.component';
import {SubscriptionsComponent} from './subscriptions/subscriptions.component';
import {HomeComponent} from './home/home.component';
import {StrategiesComponent} from './strategies/strategies.component';
import {LeaguesComponent} from './leagues/leagues.component';
import {GetStartedComponent} from './get-started/get-started.component';
// import {MainGuardService} from './main-guard.service';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    // canActivate: [MainGuardService],
    children: [
      {
        path: 'subscriptions',
        component: SubscriptionsComponent
        // canActivate: [MainGuardService]
      },
      {
        path: 'home',
        component: HomeComponent
       // canActivate: [MainGuardService]
      },
      {
        path: 'teams',
        loadChildren: () => import('./teams/teams.module').then(m => m.TeamsModule),
       // canActivate: [MainGuardService]
      },
      {
        path: '',
        redirectTo: '/get-started',
        pathMatch: 'full'
      },
      {
        path: 'games',
        loadChildren: () => import('./games/games.module').then(m => m.GamesModule),
        // canActivate: [MainGuardService]
      },
      {
        path: 'get-started',
        component: GetStartedComponent
        // canActivate: [MainGuardService]
      },
      {
        path: 'strategies',
        component: StrategiesComponent
        // canActivate: [MainGuardService]
      },
      {
        path: 'choose-leagues',
        component: LeaguesComponent
        // canActivate: [MainGuardService]
      },
      {
        path: 'sign-in',
        loadChildren: () => import('./user/user.module').then(m => m.UserModule),
        // canActivate: [MainGuardService]
      },
      {
        path: 'sign-up',
        loadChildren: () => import('./user/user.module').then(m => m.UserModule),
        // canActivate: [MainGuardService]
      },
      {
        path: 'admin',
        loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  declarations: [],
  exports: [RouterModule],
  //providers: [MainGuardService],
})
export class MainRoutingModule {
}
