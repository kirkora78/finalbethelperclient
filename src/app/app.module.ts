import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';

import {MainModule} from './main/main.module';
import {AppRoutingModule} from './app-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {RouterModule} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

// const modules = [
//   MatButtonModule,
//   MatFormFieldModule,
//   MatInputModule,
//   MatRippleModule
// ];
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgxDatatableModule,
    NgbModule,
    NgbModule,
    BrowserModule,
    MainModule,
    // GetStartedModule,
    //Ng4LoadingSpinnerModule.forRoot(),
    AppRoutingModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    MDBBootstrapModule
  //  ...modules
    // AngularFireModule.initializeApp(environment.firebase),
    //AngularFireDatabaseModule,
    //AngularFireAuthModule
  ],
  exports: [

  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class AppModule { }
