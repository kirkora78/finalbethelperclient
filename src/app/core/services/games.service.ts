import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '../../shared/app.settings';
import {Observable, throwError, of } from 'rxjs';
import {map, catchError} from "rxjs/operators";
import { GameSingleFullView } from 'src/app/core/types/server/game';

@Injectable()
export class GamesService {


  constructor(private http: HttpClient) {
  }
  getAllGames() {
    return this.http.get(AppSettings.API_ENDPOINT + 'Games/ShortAll');
  }
  getGameById(gameId: string): Observable<GameSingleFullView> { 
    return this.http.get<GameSingleFullView>(AppSettings.API_ENDPOINT.concat("Games/SingleFullView/", gameId));
    // .pipe(
    //   catchError(this.handleError<GameSingleFullView>('searchHeroes'))
    // );
  }
}
