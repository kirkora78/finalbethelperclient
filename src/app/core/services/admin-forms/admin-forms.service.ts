import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppSettings } from '../../../shared/app.settings';
import { UpdateDuplicateProfileModel } from '../../types/server/admin-forms/updateduplicateprofile';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'content-type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AdminFormsService {
  private baseUrl = AppSettings.API_ENDPOINT + 'api/ContentAdmin/';
  constructor(private http: HttpClient) {
  }

  updateMilestone(data): Observable<any> {
    const url = this.baseUrl + 'UpdateMilestone?id=' + data.id;
    return this.http.put(url, data, httpOptions);
  }

  updateDuplicateProfile(data): Observable<any> {
    const url = this.baseUrl + 'UpdateDuplicateProfile';
    return this.http.post(url, data, httpOptions);
  }

  updateUndefinedProfile(data): Observable<any> {
    const url = this.baseUrl + 'UpdateUndefinedProfile';
    return this.http.post(url, data, httpOptions);
  }

  updateCurrentFormLevel(data): Observable<any> {
    const url = this.baseUrl + 'UpdateCurrentFormLevel?id=' + data.id + '&newContent=' + data.newContent;
    return this.http.put(url, data, httpOptions);
  }

  updateCurrentFormDetails(data): Observable<any> {
    const url = this.baseUrl + 'UpdateCurrentFormDetails?id=' + data.id + '&newContent=' + data.newContent;
    return this.http.put(url, data, httpOptions);
  }

  updateManager(data): Observable<any> {
    const url = this.baseUrl + 'UpdateManager';
    return this.http.put(url, data, httpOptions);
  }

  updateProfilePostsandPenalties(data): Observable<any> {
    const url = this.baseUrl + 'UpdateProfilePostsandPenalties';
    return this.http.put(url, data, httpOptions);
  }

  updateGameStatus(data): Observable<any> {
    const url = this.baseUrl + 'UpdateGameStatus';
    return this.http.put(url, data, httpOptions);
  }

  updatePlayerAbsence(data): Observable<any> {
    const url = this.baseUrl + 'UpdatePlayerAbsence';
    return this.http.put(url, data, httpOptions);
  }
}
