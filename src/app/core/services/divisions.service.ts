import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '../../shared/app.settings';

@Injectable()
export class DivisionsService {


  constructor(private http: HttpClient) {
  }
  getAllDivisionsByCountry(countryName: string) {
    return this.http.get(AppSettings.API_ENDPOINT + 'api/Divisions/GetAllDivisions/' + countryName);
  }

}
