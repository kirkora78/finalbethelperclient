import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AppSettings} from '../../shared/app.settings';



@Injectable()
export class HomeService {


  constructor(private http: HttpClient) {
  }
  getAllCountries() {
    return this.http.get(AppSettings.API_ENDPOINT + 'Countries/All');
  }
}
