import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AppSettings} from '../../shared/app.settings';


@Injectable()
export class TeamsService {


  constructor(private http: HttpClient) {
  }
  getAllTeams() {
    return this.http.get(AppSettings.API_ENDPOINT + 'api/Teams/GetAll/league-two');
  }
  getTeamById(nick: string,skip:number,take:number) {
    return this.http.get(AppSettings.API_ENDPOINT.concat('api/Teams/GetSingle/' + nick+'?skip='+skip+'&take='+take));
  }
  getTeamsByDivisionId(divisionName: string) {
    return this.http.get(AppSettings.API_ENDPOINT + 'api/Teams/GetAll/' + divisionName);
  }
}
