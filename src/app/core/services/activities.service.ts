import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppSettings } from '../../shared/app.settings';

const httpOptions = {
  headers: new HttpHeaders({
    'content-type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ActivitiesService {

  constructor(private http: HttpClient) {
  }
  getActivities(model) {
    return this.http.post(AppSettings.API_ENDPOINT + 'api/activities/Get', model, httpOptions);
  }
}
