export class User {
  public id: number;
  public userName: string;
  public password: string;
  public firstName: string;
  public lastName: string;
  // public role: UserRole;
  public email: string;

  constructor(userName: string, password: string, firstName: string, lastName: string, email: string) {
    this.email = email;
    this.userName = userName;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
  }
}
