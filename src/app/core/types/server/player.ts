import { ActivityProfileModel } from "src/app/core/types/server/activity";
import { EmptyActivity } from "src/app/core/types/server/EmptyActivity";

export interface PlayerProfileModel {
    playerId:string
    playerName:string
    position:string
    playerNumber:string
    currentGameMinPlayes:string
    dateFirstAppearance :string
    currentActivity:ActivityProfileModel
    activities:ActivityProfileModel[]
}

export interface NewPlayerEntryPushModel{
    playerName : string
    playerNumber:string

    currentMinutesPerGame:string
    Activities:EmptyActivity[]
    gameDate:string
}