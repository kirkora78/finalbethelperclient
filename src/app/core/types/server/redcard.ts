export interface    RedCardEventModel{
    redCardProfileEventId:number
    periodInMinutes:number
    countCurrentPlayersDifference:number
    resultReceiving:string
    redCardPlayStatus:string
    gameProfileId:number
}

export interface RedCardModel{
    activityId:number
    muniteReceived:string
    resultReceived:string
}