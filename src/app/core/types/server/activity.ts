import { RedCardModel } from "src/app/core/types/server/redcard";
import { GameStatus } from "src/app/core/types/server/game";

export interface ActivityProfileModel {
    activityId:number
    allAssists:number
    goals:number
    goalAssists:number
    redCard:RedCardModel 
    minutesPerGame:string
    allShots:number
    bars:number
    goalLines:number
    activityDate:Date
    gameStatus:GameStatus
    playerCurrentPosition:string
    profileId:number
    playerId:string
    isInjSub:boolean
    subResult:string
    isMileStone:boolean
    isKeyMissing:boolean
    PS:number
    isMissing:boolean
    missingReason:string
}

