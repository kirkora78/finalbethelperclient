export interface UpdateDuplicateProfileModel {
    FirstPlayerId: number;
    FirstPlayerName: string;
    SecondPlayerId: number;
    SecondPlayerName: string;
}
