export interface Division {
  name: string;
  country: string;
}
