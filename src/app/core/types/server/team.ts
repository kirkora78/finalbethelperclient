import { PlayerProfileModel, NewPlayerEntryPushModel } from 'src/app/core/types/server/player';
import { RedCardEventModel } from 'src/app/core/types/server/redcard';

export interface TeamRealTimeView {
    name: string;
    nick: string;
    teamProfiles: TeamProfileModel [];
    currentProfile: TeamProfileModel;
    players: PlayerProfileModel[];
    newPlayers: NewPlayerEntryPushModel[];
 }
  export interface TeamProfileModel {
    gameProfileId: number;
    ownGoals: string;
    otherGoals: string;
    matchSituation: string;
    resultOutcomeLetter: string;
    rivalName: string;
    missedPen: number;
    savedPen: number;
    goalLines: number;
    isSelected: boolean;
    playersMatchSchema: string;
    gameDateDay: string;
    gameDateMonth: string;
    newManager: string;
    currentFormLevel: number;
    currentRivalFormLevel: number;
    currentFormDetails: string;
    currentFormRivalDetails: string;
    isMileStone: boolean;
    visibilityMode: boolean;
    woodWorks: string;
    redCardEvents: RedCardEventModel[];
    playersIds: string[];
  }

  export interface SingleTeamModel {
    currentProfile: Object;
    name: string;
    newPlayers: any;
    players: Array<any>;
    teamGameProfiles: Array<any>;
  }
