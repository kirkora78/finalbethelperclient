import { TeamRealTimeView } from "src/app/core/types/server/team";

export interface GameSingleFullView {
    gameId: string
    status: string
    dateForGame: Date
  //hostName: string
  //guestName: string
    hostModel: TeamRealTimeView
    guestModel: TeamRealTimeView  
 }

 export enum GameStatus{
    Archived, Fixture, InProgress, PostPoned
 }
